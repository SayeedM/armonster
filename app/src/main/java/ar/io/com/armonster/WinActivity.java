package ar.io.com.armonster;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by sayeedm on 9/22/16.
 */
public class WinActivity extends Activity {

    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.you_won);

        findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
