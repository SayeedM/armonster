/*
 * Copyright (C) 2014 BeyondAR
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.io.com.armonster;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.beyondar.android.fragment.BeyondarFragmentSupport;
import com.beyondar.android.plugin.radar.RadarView;
import com.beyondar.android.plugin.radar.RadarWorldPlugin;
import com.beyondar.android.view.BeyondarGLSurfaceView;
import com.beyondar.android.view.OnClickBeyondarObjectListener;
import com.beyondar.android.view.OnTouchBeyondarViewListener;
import com.beyondar.android.world.BeyondarObject;
import com.beyondar.android.world.World;

public class CameraWithTouchEventsActivity extends FragmentActivity implements
		OnTouchBeyondarViewListener, OnClickBeyondarObjectListener {

	private BeyondarFragmentSupport mBeyondarFragment;
	private World mWorld;

	private TextView mLabelText;
	private TextView mScoreText;
	private int monsterKilled = 0;
	private RadarView mRadarView;
	private RadarWorldPlugin mRadarPlugin;


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Hide the window title.
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		loadViewFromXML();


		mRadarView = (RadarView) findViewById(R.id.radarView);
		// Create the Radar plugin
		mRadarPlugin = new RadarWorldPlugin(this);
		// set the radar view in to our radar plugin
		mRadarPlugin.setRadarView(mRadarView);
		// Set how far (in meters) we want to display in the view
		mRadarPlugin.setMaxDistance(100);

		// We can customize the color of the items
		mRadarPlugin.setListColor(CustomWorldHelper.LIST_TYPE_EXAMPLE_1, Color.RED);
		// and also the size
		mRadarPlugin.setListDotRadius(CustomWorldHelper.LIST_TYPE_EXAMPLE_1, 3);




		// We create the world and fill it
		mWorld = CustomWorldHelper.generateObjects(this);

		mBeyondarFragment.setWorld(mWorld);

		// add the plugin
		mWorld.addPlugin(mRadarPlugin);

		mBeyondarFragment.showFPS(true);

		// set listener for the geoObjects
		mBeyondarFragment.setOnTouchBeyondarViewListener(this);
		mBeyondarFragment.setOnClickBeyondarObjectListener(this);



	}

	private void loadViewFromXML() {
		setContentView(R.layout.camera_with_text);
		mBeyondarFragment = (BeyondarFragmentSupport) getSupportFragmentManager().findFragmentById(
				R.id.beyondarFragment);

		mLabelText = (TextView) findViewById(R.id.labelText);
		mScoreText = (TextView) findViewById(R.id.score);
	}

	@Override
	public void onTouchBeyondarView(MotionEvent event, BeyondarGLSurfaceView beyondarView) {

		float x = event.getX();
		float y = event.getY();

		ArrayList<BeyondarObject> geoObjects = new ArrayList<BeyondarObject>();

		// This method call is better to don't do it in the UI thread!
		beyondarView.getBeyondarObjectsOnScreenCoordinates(x, y, geoObjects);

		String textEvent = "";

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			textEvent = "Event type ACTION_DOWN: ";
			break;
		case MotionEvent.ACTION_UP:
			textEvent = "Event type ACTION_UP: ";
			break;
		case MotionEvent.ACTION_MOVE:
			textEvent = "Event type ACTION_MOVE: ";
			break;
		default:
			break;
		}

		Iterator<BeyondarObject> iterator = geoObjects.iterator();
		while (iterator.hasNext()) {
			BeyondarObject geoObject = iterator.next();
			textEvent = textEvent + " " + geoObject.getName();

		}
		mLabelText.setText("Event: " + textEvent);
	}

	@Override
	public void onClickBeyondarObject(ArrayList<BeyondarObject> beyondarObjects) {
		System.out.println("Object count " + beyondarObjects.size());
		if (beyondarObjects.size() > 0) {
			beyondarObjects.get(0).setVisible(false);
            CustomWorldHelper.removeObject(beyondarObjects.get(0));
			monsterKilled++;
			mScoreText.setText("Monster Killed : " + monsterKilled);

			if (monsterKilled == 9){


				Intent ii = new Intent(CameraWithTouchEventsActivity.this, WinActivity.class);
				startActivity(ii);
				CustomWorldHelper.destroy();
				finish();

			}

//			Toast.makeText(this, "Clicked on: " + beyondarObjects.get(0).getName(),
//					Toast.LENGTH_LONG).show();
		}
	}

}
